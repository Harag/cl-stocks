(in-package :cl-stocks)

(defparameter *wallet* 1000)
(defparameter *random-buy* 50)

(defparameter *stocks* '(:gold
			 :silver
			 :platinum
			 :copper
			 :palladium
			 ))

(defparameter *stock-prices-previous* nil)

(defparameter *stock-prices*
  '((:stock :gold
     :price 10)
    (:stock :silver
     :price 10)
    (:stock :platinum
     :price 10)
    (:stock :copper
     :price 10)
    (:stock :palladium
     :price 10)))



(defparameter *players*
  '((:name "Piet Snot"
     :wallet *wallet*
     :stocks ((:stock :palladium :count 0) (:stock :platinum :count 0)
	       (:stock :gold :count 0) (:stock :copper :count 0)))
    (:name "Gert Gieter"
     :wallet *wallet*
     :stocks ((:stock :palladium :count 0) (:stock :platinum :count 0)
	       (:stock :gold :count 0) (:stock :copper :count 0)))))

(defparameter *cycles*
  '(:cycles 12
    :length 5))

(defparameter *random-players-old* nil)

(defparameter *random-players-previous* nil)

(defparameter *random-players* nil)

(defun create-random-players (how-many)
  (setf *random-players* nil)
  (dotimes (x how-many)
    (push (list :name x :wallet *wallet*)
	  *random-players*)))


(defun get-price-stock (name)
  (dolist (stock *stock-prices*)
    (when (equalp (getf stock :stock)
		  name)
      (return-from get-price-stock stock))))


(defun worst-performer ()
  (let ((worst)
	(worst-stock)
	(none))
    (dolist (stock *stock-prices*)
      (let ((percentage (stock-price-diff-% (getf stock :stock))))

	(if (= percentage 1)
	    (push nil none)
	    (push t none))
	(if (not worst)
	    (progn
	      (setf worst percentage)
	      (setf worst-stock stock))
	    (if (< percentage worst )
		(progn
		  (setf worst percentage)
		  (setf worst-stock stock))))))
    (unless (every #'not none)
      worst-stock)))

(defun get-stock-price (name)
  (getf (get-price-stock name) :price))

(defun random-sell (player)
	
  (let ((cash (getf player :wallet))
	(stocks)
	(worst-stock (worst-performer)))
    
    (dolist (player-stock (getf player :stocks))
      (let ((stock-diff (stock-price-diff-% (getf player-stock :stock))))
	

	(if (or
	     (and worst-stock (> (random 100) 98))
	     (and
	      worst-stock
	      (equalp (getf worst-stock :stock)
		      (getf player-stock :stock)))		
	     (< stock-diff 0.98))
	    (let ((price (get-stock-price (getf player-stock :stock))))
	     
	      (incf cash (* (getf player-stock :count)
			    price)))
	    (push (copy-list player-stock) stocks ))))

    
    (setf player (list :name (getf player :name)
		       :wallet cash
		       :stocks stocks))))

(defun skew-popular (percentage)
  (cond ((and (< -0.9 percentage)
	      (> -0.7 percentage))
	 -4)
	((and (< -0.7 percentage)
	      (> -0.5 percentage))
	 -3)
	((and (< -0.5 percentage)
	      (> -0.3 percentage))
	 -2)
	((and (< -0.3 percentage)
	      (> -0.2 percentage))
	 -1.5)
	((and (< -0.2 percentage)
	      (> -0.1 percentage))
	 -1)
	((and (< 0.9 percentage)
	      (> 0.7 percentage))
	 4)
	((and (< 0.7 percentage)
	      (> 0.5 percentage))
	 3)
	((and (< 0.5 percentage)
	      (> 0.3 percentage))
	 2)
	((and (< 0.3 percentage)
	      (> 0.2 percentage))
	 1.5)
	((and (< 0.2 percentage)
	      (> 0.1 percentage))
	 1)
	(t
	 1)))


(defun add-stock (player bought-stock)
  (let ((found-p nil)
	(updated-stock))

     (dolist (stock (getf player :stocks))
 
      (when (equalp (getf stock :stock)
		    (getf bought-stock :stock))
	(setf found-p t)
	
	(setf updated-stock (list :stock (getf stock :stock)
				  :count (+ (getf stock :count)
					    (getf bought-stock :count))))
	(setf (getf player :stocks) (remove stock (getf player :stocks)))
	(setf (getf player :stocks) (push updated-stock (getf player :stocks)))))
    
    (unless found-p
      (setf (getf player :stocks) (push bought-stock (getf player :stocks))))

    player))

(defun add-stocks (player stocks)
  (dolist (stock stocks)
    (setf player (add-stock player stock)))
  player)

(defun nshuffle (sequence)
  (loop for i from (length sequence) downto 2
        do (rotatef (elt sequence (random i))
                    (elt sequence (1- i))))
  sequence)

(defun random-buy (player)
  (let ((cash (getf player :wallet))
	(bought-stocks)
	(playerx)
	(worst-stock (worst-performer)))
    
    (dolist (stock (nshuffle *stock-prices*))
      (when (or
	     (and worst-stock (> (random 100) 98))
	     (not worst-stock)
	     (not (equalp (getf worst-stock :stock)
			  (getf stock :stock))))
	(let* ((stock-diff (stock-count-diff-% (getf stock :stock)))
	       (stock-count (round (* (random *random-buy*) (skew-popular stock-diff))))
	       (wanted (* stock-count (getf stock :price))))

	  (if (> cash wanted)
	      (progn
		(setf cash (- cash wanted))
		(setf bought-stocks (push (list :stock (getf stock :stock)
						:count stock-count)
					  bought-stocks)))
	      (if (> cash 0)
		  (if (>= 1 (/ cash (getf stock :price)))		    
		      (progn
			(setf stock-count (round (/ cash (getf stock :price))))
			(setf cash (- cash (* stock-count (getf stock :price))))
			(setf bought-stocks (push (list :stock (getf stock :stock)
							:count stock-count)
						  bought-stocks)))))))))

    (if bought-stocks
      (setf playerx (add-stocks (list :name (getf player :name)
				      :wallet (getf player :wallet)
				      :stocks (copy-list (getf player :stocks)))
				bought-stocks))
      (setf playerx (list :name (getf player :name)
				 :wallet (getf player :wallet)
				 :stocks (copy-list (getf player :stocks)))))
    
    (setf (getf playerx :wallet) cash)

    playerx))


(defun random-sells ()
  (let ((players))
    (dolist (player *random-players*)
      (push (random-sell player) players))
    (setf *random-players* players)))

(defun random-buys ()
  (let ((players))
    (dolist (player *random-players*)
       (let ((playerx (random-buy player)))
	
	(push playerx players)))
    (setf *random-players* players)))

(defun stock-count (players stock-name)
  (let ((stock-count 0))
    (dolist (player players)
      (dolist (player-stock (getf player :stocks))
	(when (equalp stock-name 
		      (getf player-stock :stock))
	  (incf stock-count (getf player-stock :count)))))
    stock-count))

(defun stock-count-diff (stock-name)
  (let ((stock-count (stock-count *random-players-previous* stock-name))
	(old-stock-count (stock-count *random-players-old* stock-name)))
    (- stock-count old-stock-count)))

(defun stock-count-diff-% (stock-name)
  (let ((stock-count (stock-count *random-players-previous* stock-name))
	(old-stock-count (stock-count *random-players-old* stock-name)))
    (if (= old-stock-count 0)
	1
	(/ stock-count old-stock-count))))

(defun get-price-stock-previous (name)
  (dolist (stock *stock-prices-previous*)
    (when (equalp (getf stock :stock)
		  name)
      (return-from get-price-stock-previous stock))))

(defun get-previous-stock-price (stock-name)
  (getf (get-price-stock-previous stock-name) :price))


(defun stock-price-diff (stock-name)
  (- (get-previous-stock-price stock-name)
     (get-stock-price stock-name)))

(defun stock-price-diff-% (stock-name)
  (unless (equalp (get-stock-price stock-name)
		  (get-previous-stock-price stock-name)))
  (/ (get-stock-price stock-name)
     (get-previous-stock-price stock-name)))

(defun set-new-prices ()
  (let ((stock-prices)
	(margin (/ 0.001 (length *stock-prices*))))
    
    (dolist (stock *stock-prices*)
      (let* ((stock-diff (stock-count-diff (getf stock :stock)))
	    (new-price (+ (getf stock :price)
			      (if (<= stock-diff 0)
				  (* -1 margin
				     (if (> (random 100) 98)
					 (* -1 (random 1000))
					 1000))
				  (* stock-diff
				     (if (> (random 100) 80)
					 (* margin (random 15))
					 margin
					 ))))))

	(push (list :stock (getf stock :stock)
		    :price (if (< new-price 2)
			       2
			       new-price) )
	      stock-prices)))
    (setf *stock-prices* stock-prices)))


(defun show-count ()
  (let ((stock-counts))
    (dolist (stock *stock-prices*)
      (let ((stock-count 0))
	(dolist (player *random-players*)
	  (print player)
	  (dolist (player-stock (getf player :stocks))
	    (when (equalp (getf stock :stock)
			  (getf player-stock :stock))
	      (incf stock-count (getf player-stock :count)))))
	(push (list :stock (getf stock :stock) :count stock-count)
	      stock-counts)))
    stock-counts))

(defun run-cycle (how-many &key new-run-p)
  
  (when new-run-p
    (setf *stock-prices-previous* (copy-list *stock-prices*))
    (create-random-players how-many))
  
  (random-sells)

  
  (unless new-run-p
    (setf *random-players-old* *random-players-previous*)
    (setf *random-players-previous* (append (copy-list  *random-players*)
					    (copy-list *players*))))

  
  (random-buys)

  (setf *stock-prices-previous* (copy-list *stock-prices*))

 ;;  (print (show-count))
  (unless new-run-p
    (set-new-prices)))


(defun do-cycles (how-many)

  
  (dotimes (x how-many)
    (format t "~%")
    (print (run-cycle 0))))

(defun settle-player (player)
  (let ((wallet (getf player :wallet)))
    (dolist (stock *stock-prices*)
      (dolist (player-stock (getf player :stocks))
	(when (equalp (getf stock :stock)
		      (getf player-stock :stock))	
	  (incf wallet (* (getf stock :price) (getf player-stock :count)))
	  (setf (getf player-stock :count) 0))))
    (setf (getf player :wallet) wallet)
    player))

(defun buy-player (player)
  (let ((wallet (getf player :wallet)))
    (dolist (stock *stock-prices*)
      (dolist (player-stock (getf player :stocks))
	(when (equalp (getf stock :stock)
		      (getf player-stock :stock))
	  (if (>= wallet (* -1 (getf stock :price) (getf player-stock :count)))
	      (incf wallet (* -1 (getf stock :price) (getf player-stock :count))))
	  )))
    (setf (getf player :wallet) wallet)
    player)
  )

(defun settle-players ()
  (dolist (player *players*)
    (setf player (settle-player player)))
  *players*)

(defun buy-players ()
  (dolist (player *players*)
    (setf player (buy-player player)))
  *players*)

